# CFG inline #

1. cl_disablehtmlmotd 1;cl_forcepreload 0;fps_max 600;mat_queue_mode 2;r_drawtracers_firstperson 0;r_eyegloss 0;r_eyemove 0;r_eyeshift_x 0;r_eyeshift_y 0;r_eyeshift_z 0;r_eyesize 0;func_break_max_pieces 0;r_cheapwaterend 1;<br>
2. r_cheapwaterstart 1;mat_hdr_enabled 0;gameinstructor_enable 0;cl_autowepswitch 0;cl_autohelp 0;cl_showhelp 0;cl_disablefreezecam 1;cl_showfps 0;bind e +use;<br>
3. bind "CapsLock" "use weapon_knife; use weapon_flashbang";<br>
4. bind "4" "use weapon_smokegrenade";<br>
4. bind "c" "use weapon_molotov; use weapon_incgrenade";<br>
   bind "i" "use weapon_c4; drop";<br>
4. bind "x" "use weapon_hegrenade"; bind "shift" "+speed"; bind "CTRL" "+duck"; bind "f" "r_cleardecals"; bind "h" "+lookatweapon";<br>
5. viewmodel_presetpos 3;cl_bob_lower_amt 0;cl_bobamt_lat 0;cl_bobamt_vert 0;cl_bobcycle 0;cl_viewmodel_shift_left_amt 0;cl_viewmodel_shift_right_amt 0;gameinstructor_enable 0;cl_showhelp 0;cl_autohelp 0;cl_disablefreezecam 1;<br>
6. cl_disablehtmlmotd 1;rate 786432;cl_cmdrate 128;cl_updaterate 128;cl_interpolate 1;cl_interp 1;cl_interp_ratio 2;cl_lagcompensation 1;cl_predict 1;cl_predictweapons 1;net_maxroutable 1200;mat_monitorgamma 1.6;<br>
7. mat_monitorgamma_tv_enabled 0;voice_scale 0.4;snd_mixahead 0.025;fps_max_menu 60;net_graph 1;net_graphheight 990;net_graphmsecs 400;net_graphpos 2;net_graphproportionalfont 0;net_graphshowinterp 1;net_graphshowlatency 1;<br>
8. net_graphsolid 1;net_graphtext 1;net_maxroutable 1200;net_scale 5;r_drawtracers_firstperson 0;cl_use_opens_buy_menu 0;mm_dedicated_search_maxping 50;con_enable 1; m_rawinput 1;m_mouseaccel2 0;m_mouseaccel1 0;m_customaccel 0;<br>
9. sensitivity 1.51;<br>
10. alias "+scorenet" "+showscores; net_graphheight 0";<br>
11. alias "-scorenet" "-showscores; net_graphheight 9999";<br>
12. bind "TAB" "+scorenet";<br>
13. cl_radar_always_centered 0;cl_radar_scale 0.3;cl_hud_radar_scale 1.15;<br>
10. cl_radar_icon_scale_min 1;cl_radar_rotate 0;cl_crosshair_drawoutline 1;cl_crosshair_dynamic_maxdist_splitratio 0.35;cl_crosshair_dynamic_splitalpha_innermod 1;cl_crosshair_dynamic_splitalpha_outermod 0.5;<br>
11. cl_crosshair_dynamic_splitdist 7;cl_crosshair_friendly_warning 1;cl_crosshair_outlinethickness 1;cl_crosshair_sniper_show_normal_inaccuracy 0;cl_crosshair_sniper_width 1;cl_crosshair_t 0;cl_crosshairalpha 0.000000;cl_crosshaircolor 5;<br>
12. cl_crosshaircolor_b 255;cl_crosshaircolor_g 255;cl_crosshaircolor_r 255;cl_crosshairdot 0;cl_crosshairgap -2;cl_crosshairgap_useweaponvalue 0;cl_crosshairsize 1.5;cl_crosshairstyle 4;cl_crosshairthickness 0.5;cl_crosshairusealpha 1;<br>
13. cl_fixedcrosshairgap -1;bind alt "+jump;-attack;-attack2;-jump";bind del "callvote StartTimeOut";bind "pgdn" "buy vest;";bind "end" "buy deagle;";<br>
    bind "pgup" "buy vesthelm; buy vest;";<br>
    bind "home" "buy m4a1; buy ak47;";<br>
14. bind "uparrow" "buy flashbang;";bind "downarrow" "buy smokegrenade;";bind "rightarrow" "buy incgrenade;";bind "leftarrow" "buy hegrenade;";bind "f1" "toggle cl_radar_scale 0.3 1.3";bind "f2" "toggle cl_righthand 0 1";bind o radio;<br>